// /** @type {import('@sveltejs/kit').Handle} */
// export const handle = async ({ event, resolve }) => {
// 	return resolve(event);
// };

// /** @type {import('@sveltejs/kit').HandleServerError} */
// export async function handleError({ error, event }) {
// 	console.log(error);

// 	return {
// 		message: 'YIKES!!!'
// 	};
// }

// /** @type {import('@sveltejs/kit').HandleFetch}  */
// export async function handleFetch({ request, fetch }) {
// 	if (request.url.startsWith('http')) {
// 		const url = request.url.replace('http://', 'https://');
// 		request = new Request(url, request);

// 		console.log(request.url);
// 	}

// 	return fetch(request);
// }

// import { parseFormData } from 'parse-nested-form-data';

// /** @type {import('@sveltejs/kit').Handle} */
// export async function handle({ event, resolve }) {
// 	if (event.request.method === 'POST') {
// 		const formData = await event.request.formData();
// 		const data = parseFormData(formData);
// 		event.locals.formData = data;
// 	}

// 	return resolve(event);
// }

import { sequence } from '@sveltejs/kit/hooks';

/** @type {import('@sveltejs/kit').Handle} */
const auth = async ({ event, resolve }) => {
	console.log('auth hook');
	return resolve(event);
};

/** @type {import('@sveltejs/kit').Handle} */
const i18n = async ({ event, resolve }) => {
	console.log('i18n hook');
	return resolve(event);
};

export const handle = sequence(auth, i18n);
